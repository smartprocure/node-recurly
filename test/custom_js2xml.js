var Js2xml = require('js2xml').Js2Xml;

// there might be others
var map = {
    'subscription_add_ons': 'subscription_add_on',
    'line_items': 'adjustment'
};

Js2xml.prototype.pluralisation = function(name) {
    return map[name] || 'item';
};

var data = {
    quantity: 2,
    subscription_add_ons: [
        {
            add_on_code: 'test',
            quantity: 5
        },
        {
            add_on_code: 'weee',
            quantity: 7
        }
    ],
    line_items: [
        {
            test: 4
        }
    ]
};

var test = new Js2xml('subscription', data);
console.log(test.toString());